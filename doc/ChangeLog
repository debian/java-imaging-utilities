2009-08-03
o Added experimental support for using the Eclipse compiler from scons. Surprisingly, this seems to produce significantly faster code that a standard jdk6 javac compiler.
o README: Updated defunct URI, removed FreshMeat section, and added some information about the new maintainer (yours truly).

2009-08-02
o ReduceRGB: ensured that the output image is not null after processing.
o Changed some URIs from the now-defunct web site (schmidt.devlib.org) to SourceForge. 

2009-08-01
o JIU is back on SourceForge, currently maintained by Knut Arild Erstad.
o Made the code available in a Mercurial repository: http://jiu.hg.sourceforge.net/hgweb/jiu
o Added SCons build rules as an alternative to the .bat files.
+ PNMCodec: Added support for reading binary PPM files with 16-bit color depth per channel.

2007-04-20
+ JiuHelloWorld, JiuBlue, JiuCountColors: Added three sample programs to the apps package, they are documented on the new tour page http://schmidt.devlib.org/jiu/introduction.html
- CountColors: Removed the standalone color counting program, the new JiuCountColors sample does basically the same thing, but with the introductory tour's images

2007-04-19
o create-release.bat: Added the resources directory in the call to tar; resources were included only in the JAR file. Thanks to Stuart Prescott of http://latexdraw.sf.net/ for pointing this out!
o Resample: Added modifications by an anonymous contributor which significantly speed up image scaling with this class.
o TextureAnalysis: Added static convenience methods compute for matrices and images.
o MemoryCoOccurrenceMatrix: Made private attributes final to emphasize that they are only modified in the constructor. Added warning about 16 bit image channels.
o docs-html-options, create-release.bat: Changed JIU version to 0.14.2.
o RGBToGrayConversion: Added static convenience method convert for users who don't want to change color weights or reuse output image objects.	

2007-03-04
o UnsharpMaskKernel: Fixed bug that prevented constructor running smoothly (parameter array in call to super constructor too small). Reported by knuta: <http://sourceforge.net/forum/message.php?msg_id=4113340>.
o PSDCodec: Fixed compiler warnings by removing unused variables.
o AwtOperationProcessor: Fixed compiler warnings by replacing calls to Dialog.show with calls to Dialog.setVisible(true).
o ArrayRotation: Commented out unused private methods.
o ArrayScaling: Fixed compiler warning by removing unused local variable.
o Dialogs: Replaced call to Dialog.show with call to Dialog.setVisible(true).
o JiuAwtFrame: Replaced show with setVisible(true), declared serialVersionUID.
o OrderedDither: Fixed compiler warning about unused variable.
o OctreeColorQuantizer: Removed unused variable.

2007-01-03
o Contrast: added static convenience method, updated documentation
o Histogram1DCreator: added an additional count method for an arbitrary channel
o MeanDifference: added a static method "compute"
o MatrixCreator: added support for Gray16Image
o OnDemandHistogram3D: rewrote it from scratch to use hash tables instead of arrays

2007-01-02
+ CountColors: command line application to demonstrate how to count colors with JIU
o Histogram1DCreator, Histogram3DCreator: added static count method
o HueSaturationValue: made this class work with RGB48Image (by supporting all flavors of RGBIntegerImage)

2006-12-16
o Released current version as 0.14.1 after a request in the Open Discussion forum.
o Brightness: added static helper method adjust
o create-api-docs.html: got it working again

2006-04-18
[processed some older Sourceforge bug reports, patches and feature requests]
o JPEGData: fixed bug which added "scans" to vector "scans" instead of parameter "scan" (reported as bug #1424545 by arno_b)
o GIFCodec: added codec.close() to end of usage example in the comment (reported as patches item #1449702 by talbes)
o ImageLoader: fixed bug which led to too many open files because codec.close() wasn't called (reported as bug #1402205 by Cameron Dorrat/cdorrat); removed unnecessary imports
o PopularityQuantizer: when the source image has less unique colors than the proposed palette size, the palette size is just set to the number of unique colors instead of throwing an exception (reported as feature request #1449716 by talbes)
o PopularityQuantizer: made variable paletteIndex change its value in method determinePalette (reported as bug #1201053 by jadesailor)
o JiuInfo: updated version to 0.15.0
o jiuawtapplet: added a serialVersionUID variable to make a warning go away

2006-04-17
o Renamed the Windows batch files to have more self-explanatory names.
o Changed create-jiu.jar.bat (formerly j.bat) to create a JAR which causes no problems with (my) JDK 1.5 installation
o README: changed website URL to http://schmidt.devlib.org/jiu/
o Released as 0.14.0 (note that some of the documents still say 0.13.0, this is just a maintenance release to deliver a correct version of jiu.jar)

2006-01-18
o Added various classes related to jiuawt in order to be able to load the sample file from within an applet
o Modified website to offer jiuawt as a frame started from within an applet: http://schmidt.devlib.org/jiu/applet.html
+ Added sample image file resources/images/image1.jpg

2005-11-02
o JiuInfo: changed version from 0.13.0 to 0.14.0
+ JPEGHeaderReader: moved the various readXYZMarker methods from JPEGCodec to this new class
o Various JPEG codec changes; the whole JPEG codec still is far from working

2005-10-30
o Changed release options:
  * only one archive with source and compiled files
  * compiled LaTeX (API, manual) only as PDF, no more DVI / PS
  * no more source code HTML files
  * downloads not available anymore from SourceForge itself
o Released as 0.13.0

2004-09-14
o Strings: added constant for French, fixed bug in determineSuitableIsoCode (ISO language code returned by Locale was always assumed to be supported by JIU)
+ fr.txt: added string translation into French, created by Vincent Jestin

2004-08-07
o renamed JPEGScanHeader to JPEGScan for consistency reasons (it's also JPEGFrame, not JPEGFrameHeader)
+ JPEGScanComponentSpecification data class
o JPEGCodec: added readStartOfScan
o docs-html-options: changed version from 0.12.0 to 0.13.0

2004-08-06
o ReduceRGB: class did nothing so far. Can now deal with 24 and 48 bpp RGB input images. Produces the minimum image type necessary for chosen destination bit depth.
o Resample: changed single line as suggested by Mike Dillon in order to remove banding artifacts
o JPEGCodec: added reading of SOF_n markers, Huffman tables
o packages: added net.sourceforge.jiu.codec.jpeg
+ JPEGHuffmanTable: data class for a single Huffman table
+ JPEGScanHeader: data class for a scan header

2004-08-05
+ Added file package.html to JPEG package. Also class JPEGConstants.
o JPEGCodec: added reading of quantization tables

2004-08-04
+ Added package for the JPEG file format: net.sourceforge.jiu.codec.jpeg
+ Added classes JPEGCodec, JPEGData, JPEGQuantizationTable.

2004-08-01
o Resample: instead of Math.round uses simple typecast from float to int; small speed-up
o JiuInfo: changed version from 0.12.0 to 0.13.0

2004-07-07
o z.bat, j.bat, d-*.bat: updated to reflect directory structure
o Released as 0.12.0.

2004-07-01
o ErrorDiffusionDithering: Added documentation, renamed setType to setTemplateType.
o some classes using ErrorDiffusionDithering: changed calls to setType to setTemplateType

2004-06-24
+ GIFCodec: added class to standard distribution so that it can be released on 2004-07-07; also see the entry of 2002-06-13
o jiuawt & others: added support to jiuawt to save images as GIF files

2003-09-06
o PNGCodec: added documentation and some smaller methods to write metadata and have more configuration options when encoding
o javatips.html: checked links, added link to style sheet

2003-09-05
o ArrayConverter: added methods setIntLE / setIntBE to write int values to a byte array
o Strings: long String arrays with jiuawt text messages in all languages removed
+ StringLoader: a class to read a Strings resource from a text file
+ en.txt, de.txt, es.txt: text files that contain the String arrays formerly stored as literals in Strings
o StringIndexConstants: added various constants
o ImageDescriptionCreator: now works with a Strings object, no longer brings its own String objects
o EditorState: modified to use StringLoader
o jiuawt: modified to work with new EditorState behaviour
o JiuAwtFrame: now calls ImageDescriptionCreator with a Strings object
o MenuIndexConstants: added FILE_SAVEAS_PNG constant
o MenuWrapper: added mapping from FILE_SAVEAS_PNG to corresponding StringIndexConstants constant
o JiuInfo: changed JIU_FEEDBACK_EMAIL to JIU_FEEDBACK_ADDRESS and set the constant to http://jiu.sourceforge.net/feedback.html
o j.bat: changed version from 0.11.0 to 0.12.0
o package.html: added package.html files for all packages that didn't have one already; now the first impression of the HTML API docs is somewhat better (or at least more complete)
o ArrayConverter: added methods setShortLE / setShortBE to write short values to a byte array
o PNGCodec: can now save the following image types: bilevel, grayscale 8 and 16 bits, paletted and RGB 24 and 48 bits
o various: modified them so that the File | Save as | Portable Network Graphics (PNG)... menu item can be used

2003-09-04
o PNGCodec: continued adding support for interlaced PNGs; now reads all basi*, basn* and s* files from the PNG test suite http://www.schaik.com/pngsuite/pngsuite.html without errors; transparency information is not yet regarded; started implementation of saving images
o z.bat: updated JIU version from 0.11.0 to 0.12.0

2003-09-03
o screenshots.html: added OS/2 screenshot by David Mediavilla
o ImageDescriptionCreator: made English the default if the requested language is unavailable
o Updated CVS repository
o PNGCodec: started adding support for reading interlaced PNGs

2003-09-02
o PNGCodec: added support for reading 1, 2 and 4 bit non-interlaced paletted PNGs, 8 and 16 bit grayscale alpha and 48 bit RGB PNGs with an alpha channel
o ArrayConverter: added method decodePacked1Bit in order to decode 1 bit paletted values from decompressed PNG data
o PNGCodec: added skip method that calls the input stream's skip method, regards the returned number of bytes actually skipped and loops until all bytes have been skipped

2003-09-01
+ PNGCodec: added a codec for the PNG format, can read almost all non-interlaced PNGs, throws away all alpha information; I took the class from the 'never finished that' directory and made it work, thanks to Eclipse's debugger; main bug was in the filtering, I always started at the second pixel, but filtering has to start with the first; for all invalid horizontal index values (-1, -2, ...) 0 is to be used
o ArrayConverter: added method convertPacked4BitIntensityTo8Bit in order to decode 4 bit grayscale values from decompressed PNG data and store those values in Gray8Image objects
o ImageLoader: added PNGCodec to the list of codecs to be used in ImageLoader

2003-08-31
o ArrayConverter: Added a method getShortBE to retrieve a short value in big endian byte order from a byte array
o TIFFCodec, TIFFDecoder, TIFFImageFileDirectory: TIFF codec can now read 48 bpp interleaved RGB file type
o docs-html-options: changed JIU version from 0.11.0 to 0.12.0

2003-08-30
o JiuInfo: changed version from 0.11.0 to 0.12.0.
+ RGB48Image: new RGB integer image type with 16 bits per sample, thus 48 bits per pixel
+ MemoryRGB48Image: implementation of RGB48Image in memory, extends MemoryShortChannelImage
+ PromotionRGB48: new promotion operation that converts Bilevel/Gray8/Gray16/Paletted8/RGB24Image to the new RGB48Image type
o AwtOperationProcessor: added implementation of method colorPromotePromoteToRgb48, a call to PromotionRGB48
o AwtMenuWrapper: added menu item for Promote to 48 bits RGB
o ImageCreator: added method Image convertToAwtImage(RGB48Image image, int alpha) to convert from the new RGB48Image to an AWT image type
o RGBA: added method convertFromRGB48 to convert short arrays to an RGBA int array
o AutoDetectColorType: integrated the new RGB48Image type; now bilevel/gray8/gray16/rgb24/paletted8 images being stored as RGB48Image are recognized and optionally converted to the lower image type
o AwtOperationProcessor: added warning when a modified image is to be closed after a user call to File | Close
o Strings: added two strings for the above change
o StringIndexConstants: added two constants for the change to rows above
o PNMCodec: adjusted to read and write 48 bpp color images as ASCII
o EqualizeHistogram: fixed bug that lead to an overflow in a computation; now long instead of int is used and the operation works on 48 bpp images
o OperationProcessor: enabled a couple of operations for RGB48Image in the isAvailable method
o OilFilter: added warning that this operation takes very long with images with 16 bits per sample
o AwtOperationProcessor: now works with RGB48Image types, but quickly runs out of memory; a data structure that is less memory-intensive than OnDemandHistogram3D would be required

2003-08-29
o Released JIU 0.11.0.

2003-08-21
o various: created new pages for the project website http://jiu.sourceforge.net, added directory website to workspace, imported webpages

2003-08-20
o various: adjusted about 30 javadoc comments so that javadoc now no longer displays warnings

2003-08-19
o MedianCutContourRemoval: finished implementation
o AwtOperationProcessor: changed method colorReduceMedianCut so that the contour removal option can now be used
o Palette: added a getSamples method that returns all samples of one channel as an array
+ MeanDifference: added this class to net.sourceforge.jiu.color.analysis; it can compute the mean difference between two images
+ PopularityQuantizer: added this quantizer which determines the N color occurring most frequently in an input image and maps the input image to those colors using ArbitraryColorQuantizer
o RGBColorList: sort method has been renamed to sortByAxis; new method sortByCounter

2003-08-18
+ Statistics: new class in net.sourceforge.jiu.util, for the computation of mean, standard deviation and variance
o MedianCutContourRemoval: wrote most of the implementation
o MedianCutQuantizer, MedianCutNode: smaller modifications

2003-08-16
o First CVS commit of JIU.

2003-08-15
- ImageArithmetic: removed that class from the package, for the time being; it was nowhere near ready
o PalettedImage: is no longer derived from IntegerImage, has no super interface anymore
+ PalettedIntegerImage: new class that combines PalettedImage and IntegerImage
o Paletted8Image: is now derived from PalettedIntegerImage instead of PalettedImage
o ReduceShadesOfGray: can now reduce Gray16Image objects
o jiuawt/JiuAwtFrame/EditorState: jiuawt now opens an image file given to it as a command line parameter
o ConvolutionKernelFilter: can now deal with 16 bits per sample (there was a & 0xff operation which had to be replaced with & MAX)

2003-08-14
o AutoDetectColorType: added support for detecting 8 bit grayscale in 16 bit grayscale images and converting them to 8 bit if desired 
o OperationProcessor: made sure that reduce to bilevel threshold is available with 16 bit grayscale images (the operation itself already supported it)
o OperationProcessor: from now on, creating co-occurrence and c.o. frequency matrices is possible only with 1 or 8 bit images (everything aboves leads to matrices that do not fit into memory; 16 bit => 65536 * 65536 int entries, 2^34 bytes; a smarter [on demand] data structure may be able to deal with that)
o MemoryCoOccurrenceMatrix: now throws an IllegalArgumentException if a dimension is specified that leads to an array with more than 2^31 entries; also added documentation

2003-08-11
o various: removed a couple of small test applications in net.sourceforge.jiu.apps; they weren't useful for the general public

2003-08-06
o various: Eclipse showed about 160 warnings about unused imports; removed those imports, added a bit documentation and corrected some code that did not follow coding conventions

2003-06-06
+ ImagesToImageOperation: new operation class that takes several input images and produces one output image

2003-03-24
o AwtOperationProcessor, ImageCanvas, EditorState, JiuAwtFrame: changed logic so that zoom factor is kept while modifying the same image
+ YesNoDialog: a dialog that asks a question and offers yes, no and cancel as buttons
o AwtOperationProcessor, StringIndexConstants, Strings: when the image was modified and not yet saved and the user wants to exit, a reminder dialog pops up and asks whether the changes are really to be discarded
o ImageCodec: fixed bug (similar to the one fixed on 2003-03-20) that uses setDataInput instead of setInputStream
o EditorState, AwtOperationProcessor: bug fixed, after File / Close now there is no way of doing Edit / Undo anymore
o PNMCodec: now loads PGM 16 bpp files, also regards bounds now when loading PGM (that wasn't happening before)

2003-03-23
+ MemoryShortChannelImage: implementation of ShortChannelImage in memory, similar to MemoryByteChannelImage
+ Gray16Image: interface that merges ShortChannelImage and GrayIntegerImage, similar to Gray8Image
+ MemoryGray16Image: grayscale 16 bit image type, similar to MemoryGray8Image
+ PromotionGray16Image: creates grayscale 16 bpp images from BilevelImage and Gray8Image objects
o PNMCodec: now saves Gray16Image objects
o AwtOperationProcessor: added PromotionGray16 operation
o AwtMenuWrapper: added PromotionGray16 menu item
o RGBA: added convertFromGray16 method
o ImageCreator: added method convertToAwtImage(Gray16Image image, int alpha)
o OperationProcessor: added knowledge of gray16 to isAvailable
o jiuawt: now supports a couple of command line arguments

2003-03-21
+ BatchProcessorOperation: added this operation that processes file lists
+ ImageLoadTester: added this extension of BatchProcessorOperation that loads each file and prints a note to stdout

2003-03-20
o ImageCodec: setFile now uses setOutputStream instead of setDataOutput; the latter has no close method which lead to 0-sized output files in some cases. Problem reported by Dan Danilin.

2003-02-28
o ByteChannelImage, MemoryByteChannelImage: removed getSamples and putSamples method that do not have the channel as first argument

2003-02-27
+ ShortChannelImage: added this interface, similar to ByteChannelImage (just with 16 bit values)

2002-11-15
+ BufferedRGB24Image: added this bridge class between AWT and JIU; a BufferedImage can now be used as a RGB24Image

2002-08-05
+ UnsharpMaskKernel: added this kernel, suggested by Niels Donvil

2002-07-30
o Resample: put the filters into classes of their own

2002-07-28
o Strings.java: added Spanish strings, thanks to Luis Salas

2002-07-26
o ErrorDiffusionDithering: removed constructor arguments, added setTemplateData
o OctreeColorQuantizer: added setPaletteSize(int) method

2002-07-25
o PixelImage: added createCopy method

2002-07-24
o java-imaging-utilities-manual.tex: added introduction chapter for developers to get into JIU faster

2002-07-23
+ SeekableByteOutputStream: extension of OutputStream that allows to seek in the output

2002-07-22
o Resample: changed default type from Box (nearest neighbor) to Triangle (bilinear)

2002-07-21
o PalmCodec: now writes scanline-compressed files

2002-07-20
o PalmCodec: now writes RLE-compressed files

2002-07-16
o PalmCodec: now writes 0xff as value in the compression field when saving images as "uncompressed"
o LogLuvConversion: added support for 24 bit LogLuv colors
o TiffDecoderLogLuv: added support for COMPRESSION_SGI_LOG_24_PACKED (34677)

2002-07-15
o PalmCodec: now writes 1, 8 and 16 bpp images (uncompressed only)
o jiuawt: added File | Save as... | Palm
o MapToArbitraryPaletteDialog: now offers the Palm system palettes

2002-07-13
o PalmCodec: started implementation of saving images

2002-07-12
o PalmCodec: resumed work on this codec, put it back into ImageLoader
o PalmCodec: added a method to create the default Palm palette
o ArrayConverter: added method decodePackedRGB565BigEndianToRGB24 required by 16 bpp Palm image files
o PalmCodec: now reads 16 bpp truecolor Palm image files

2002-07-11
+ jiuconvert: started a command line converter; this will be helpful for codec development because jiuawt involves too much clicking until one can test the actual changes to a codec

2002-07-05
o TIFFDecoder + heirs: added method getCompressionTypes that returns the TIFF compression values supported by that particular decoder
o TIFFCodec: now has an internal hash table of (compression key, decoder class) pairs that will be used to create a decoder; a new registerDecoder method lets developers register external decoders
+ TIFFDecoderLogLuv: a decoder for SGI LogLuv RLE compression (so far, only the 32 bit flavor is decoded)
+ LogLuvConversion: a conversion class from LogLuv to RGB
o TIFFDecoderLogLuv, LogLuvConversion: now also support decoding and conversion of LogL to grayscale
+ created new package net.sourceforge.jiu.color.conversion and moved the three color conversion classes there
o renamed YCbCrConversion to PCDYCbCrConversion so that it will not be mistaken for the kind of conversion that takes place with JPEG files

2002-07-04
o ImageCodec: now has new methods setFile, isLoadingSupported, isSavingSupported for (hopefully) easier use of the codecs

2002-07-03
+ CMYKConversion: performs a simplistic conversion (only direction CMYK to RGB right now)
o TIFFDecoder: now can read interleaved CMYK files
o TIFFCodec: when reading 2 to 4 BYTE values in a tag, the offset is used directly, no longer as an offset into the file
o JiuAwtFrame: when at maximum zoom level, the View | Zoom in menu item is now disabled
o JiuAwtFrame: when at minimum zoom level, the View | Zoom out menu item is now disabled
o JiuAwtFrame: when at original size zoom level (1:1), the View | Set original size menu item is now disabled

2002-07-02
o EditorState: added API documentation
o CoOccurrenceFrequencyMatrix: modified it so that javadoc outputs no more warnings (for the time being), COFM was the last class it was complaining about
o TIFFImageFileDirectory, TIFFDecoder: now grayscale and paletted images with 4 bits per pixel can be read

2002-06-30
o HistogramCreator: made two classes from it, Histogram1DCreator and Histogram3DCreator, both extend Operation with progress notification; also changed all classes depending on it

2002-06-27
o JiuInfo: changed version from 0.9.0 to 0.10.0
o MeanFilter: changed it to be an AreaFilterOperation
o MedianFilter: changed it to be an AreaFilterOperation
o OilFilter: changed it to be an AreaFilterOperation

2002-06-25
o released JIU 0.9.0
o TIFFCodec: fixed bug in TIFFCodec.load(TIFFImageFileDirectory), for some reason the tileIndex was not increased in the while loop

2002-06-24
o JiuAwtFrame now implements ComponentListener and reacts on resize events by doing a new layout of the scrollpane

2002-06-23
o TIFFDecoder: added progress notification for TIFF reading by adding a call to setProgress to TIFFDecoder's private storeRow method
o some smaller changes to make the library pass JavaPureCheck with the 1.1 level (removed Graphics2D rendering hints from ImageCanvas and jiuawt menu)

2002-06-22
o MedianCutQuantizer: changed this to be an ImageToImageOperation
o ConvolutionKernelFilter: changed this to be an ImageToImageOperation
o added View menu to jiuawt and updated various classes to make it do something

2002-06-20
- ImageFrame: removed this class which did not get used anymore (JiuAwtFrame uses ImageCanvas directly)

2002-06-19
o PNMCodec: fixed bug that created truncated PNM streams; added close() at the end of save() so that everything is flushed

2002-06-14
+ ClusteredDotDither: added new class for clustered dot dithering

2002-06-13
+ GIFCodec: created a class to write GIF files; unfortunatly, I cannot put this into the standard distribution due to LZW patenting problems
o changed package structure, added various subpackages to net.sourceforge.jiu.color

2002-06-11
+ TIFFDecoderPackbits to decompress Packbits-compressed TIFFs; adjusted TIFFCodec to use this decoder
+ TIFFDecoderModifiedHuffman to decompress Modified-Huffman-compressed TIFFs; adjusted TIFFCodec to use this decoder

2002-06-09
o TIFFDecoder: added methods to do the storing of uncompressed data; the real decoders (heirs of TIFFDecoder) can now concentrate on decompression
+ TIFFDecoderUncompressed: first real decoder, simply reads data and stores it

2002-06-06
o adjusted IFFCodec

2002-06-05
o Shear: converted it to an ImageToImageOperation
+ PixelImage: added getImageType method
o Contrast: converted it to a LookupTableOperation
+ AreaFilterOperation: mean, median and oil will be modified to extend this
+ MaximumFilter: extends AreaFilterOperation, added this to jiuawt as well
+ MinimumFilter: dto.

2002-06-03
o adjusted GammaCorrection to be a LookupTableOperation

2002-06-02
o renamed package transform to geometry

2002-06-01
o ImageCodec: added method checkImageResolution
o BMPCodec: now reads and writes pixels per meter as physical resolution
o BilevelImage, MemoryBilevelImage: cleaned up interface and memory implementation of it
o TIFFCodec: removed global variable in; replaced with local variables initialized with getRandomAccessFile()
o converted MeanFilter, MedianFilter and OilFilter to be ImageToImageOperations
o converted Crop to ImageToImageOperation

2002-05-31
o TIFFCodec: removed methods that are already in ImageCodec (setImageIndex, setInput); added process method, made load private

2002-05-28
o moved getNumComments, getComment and appendComment from PNMCodec to ImageCodec (other file formats can store comments as well)
o ImageCodec: added method removeAllComments

2002-05-16
o all classes in the net.sourceforge.codecs.tiff now have Tiff instead of TIFF in their names (naming inconsistency)

2002-05-14
o moved all TIFF-related classes to a new package net.sourceforge.jiu.codecs.tiff

2002-05-13
o RASCodec: removed load and save method
o ArbitraryPaletteQuantizer: changed it from a Operation to an ImageToImageOperation; now WrongParameterExceptions are thrown when image types are incorrect

2002-05-12
o BMPCodec: now supports loading 1, 4, 8 and 24 bpp BMP files (all uncompressed, also 4 and 8 bpp RLE compressed)

2002-05-09
+ ImageDescriptionCreator: creates a text with a textual description with the properties of a JIU image object
o changed JiuAwtFrame to use ImageDescriptionCreator instead of the image's toString() method

2002-04-25
o TransparencyInformation: removed unnecessary method

2002-04-24
o ByteChannelImage: added check for null so that there are no NullPointerExceptions in getAllocatedData

2002-04-23
o Histogram3D: removed MAX_BITS constant
o OnDemandHistogram3D: changed meaning of first three int arguments of (int, int, int, int, int, int) from "maximum number of bits" to "number of entries"
o HistogramCreator: changed argument to OnDemandHistogram3D constructor from 8 (number of bits) to 256 (number of entries)
o checked out Tuukka Pasanen's codecs that read: ICO, OTA, WBMP, XBM and XPM and adjusted ImageLoader to support those new codecs; they don't work yet, but now I'll only have to remove comment characters to activate them

2002-04-23
o BilevelImage is now an interface
+ added MemoryBilevelImage (which is just a renamed copy of the class BilevelImage which has now become an interface)
o cleaned up a lot of code; mostly calls BilevelImage img = new BilevelImage(x, y) which have now changed to BilevelImage img = new MemoryBilevelImage(x, y)

2001-12-23
+ thumb: added example program thumb that creates a JPEG thumbnail from an input image using the com.sun.image.codec.jpeg package

2001-12-22
o Operation: added methods getAbort, setAbort

2001-12-09
o ImageCodec: added abstract method String[] getMimeTypes()
o all classes extending image codec: added method String[] getMimeTypes() that returns corresponding MIME type strings for that image type

2001-12-07
o TextureAnalysis: added properties correlation, dissimilarity

2001-12-05
o JiuInfo: changed version from 0.8.0 to 0.9.0

2001-12-05
o released 0.8.0

2001-11-15
o HueSaturationValueDialog: now shows a Panel with the color of the hue to be set

2001-11-14
o EditorState: now redo and undo also take care of handling the modified flag

2001-11-13
o Invert: changed the operation to be an ImageToImageOperation
+ Moved all AWT related information from SystemInfo to new class AwtInfo in the gui.awt package
o Moved the SystemInfo class from apps to util
o Modified HistogramSerialization and added menu item Save histogram as... to jiuawt
o EditorState: added functionality for undo / redo operations; updated the AWT demo program classes

2001-11-10
+ During the last days, made major changes to packages apps and gui.awt; now, processing and GUI logic are in separate classes
o RGBToGray: replaced that class with new class RgbToGrayConversion, which has user-defineable color weights
+ PromoteToGray8: new class, works for BilevelImage objects only
+ PromoteToPaletted8: new class, works for BilevelImage and Gray8Image objects

2001-11-03
o UniformPaletteQuantizerDialog, jiuawt: adjusted to reflect changes to OrderedDither
o AutoDetectColorType: further updated class

2001-11-02
o OrderedDither: now supports dithering from RGB to RGB (with 9 or more bits per pixel)

2001-10-30
o GrayImage: added method getSampleAsFloat
o BilevelImage, Gray8Image: implemented method getSampleAsFloat as demanded by changed GrayImage interface

2001-10-27
+ HTMLTableCodec: started codec to write pixel image to an HTML file with table cells representing single pixels

2001-10-21
o JiuInfo: changed version from 0.7.0 to 0.8.0
o PCDCodec, ImageLoader: adjusted codec and loader to use the process method
o PNGCodec, ImageLoader: adjusted codec and loader to use the process method
o ScaleReplication, jiuawt: adjusted ScaleReplication to be an ImageToImageOperation, modified call in jiuawt as well

2001-10-20
o created 0.7.0 release on sourceforge, added release info on freshmeat

2001-10-17
+ started manual as docs\manual.tex

2001-10-15
+ BMPCodec: add new codec, only writing of uncompressed BMPs is supported (bilevel, gray8, paletted, rgb)
o jiuawt, Strings, StringIndexConstants: 
o ImageLoader: removed buggy MBMCodec from set of codecs to be tried

2001-10-10
+ OperationFailedException: new operation class
o Operation: added process method
o ImageCodec: added class to get and set DataIn/Output and In/OutputStream objects
o IFFCodec: adjusted methods to use process instead of load

2001-07-23
o MBMCodec: added support for 8-bits-per-pixel images

2001-07-16
+ TextureAnalysis: added class to determine contrast, energy, entropy and homogeneity of a co-occurrence matrix
o IntegerDialog: commented out console debug messages
o ErrorDiffusionDithering: changed error in docs (7 / 16 instead of 1 / 16)

2001-07-12
o JiuInfo: changed version from 0.6.0 to 0.7.0
o Operation, Parameter: changed name of used exception class MissingValueException (does not exist anymore) to MissingParameterException; thanks to Rasmus Kaj for pointing that out

2001-07-10
+ finished new Octree color image quantizer (classes OctreeColorQuantizer, OctreeNode and OcrtreeDialog)
+ added quantizer (with or without dithering) to jiuawt, under Color | Color image quantization
+ added MatrixSerialization to write text files with the content of co-occurrence matrices and co-occurrence frequency matrices
+ added matrix I/O features to jiuawt, under Color | Histogram
o released 0.6.0 on SourceForge, updated homepage and created a Freshmeat entry

2001-06-27
+ added LookupTableOperation
+ added NormalizeHistogram
+ added EqualizeHistogram
o jiuawt: fixed bug that displayed wrong file name after saving image as RAS

2001-06-26
o YCbCrConversion: changed floatToInt to use a simple (int) cast instead of Math.round
+ added net.sourceforge.jiu.ops.ImageToImageOperation; it is supposed to be the base class for all operations that convert an image into another image
o added batch file for Nicolas Zin's RTF doclet (http://www.efrei.fr/~zin/RTFDoclet.zip); however, MS Word Viewer does not like the generated RTF file (page 38 ErrorDiffusionDithering seems to be a problem)

2001-06-25
o added 0.5.0 release note to http://freshmeat.net (which is back from the dead after 24 hrs)
o JiuInfo: changed version from 0.5.0 to 0.6.0
o moved ArrayScaling and ArrayRotation to net.sourceforge.jiu.util

2001-06-24
+ ArbitraryPaletteQuantizer: maps RGB images to any given palette; relatively slow
o jiuawt: added Color | Color image quantization | Map to arbitrary palette, to map to websafe palette or palette from file; optional error diffusion dithering
o released 0.5.0, updated homepage

2001-06-23
+ added WebsafePaletteCreator
+ PaletteSerialization: load palettes from RGB images, save palettes as ASCII PNM images
o jiuawt: added Color | Palette menu
o jiuawt: added saving of palettes Color | Palette | Save as...

2001-06-21
+ added MeanFilter
+ added WindowSizeDialog (to enter a pair of odd positive numbers)
o modified jiuawt to support mean, median and oil filters
o modified predefined ConvolutionKernelFilter emboss; now, a value is added so that the embossed version is brighter
o updated MedianCutDialog so that all contour removal parameters can be entered

2001-06-20
+ added class HueSaturationValue to adjust saturation and value as relative values (and optionally set hue as an absolute value)
+ added dialog class HueSaturationValueDialog for HueSaturationValue
o adjusted jiuawt, Strings, StringIndexConstants to support hue / saturation / value
+ created package net.sourceforge.jiu.filters; moved the convolution kernel classes (filter and data) there
+ added OilFilter
+ added MedianFilter

2001-06-19
o jiuawt: fixed error with Save as PBM/PGM/PNM; now the correct name of the newly-created file is shown in the title bar
o PNMCodec: fixed bug with saving binary PBM files; the colors black and white were inverted and are now correct
o jiuawt: fixed bug; applying a convolution kernel filter now leads to a modified flag (asterisk in title bar)

2001-06-18
+ added class ConvolutionKernelData to store all information for a convolution kernel filtering operation like blur, sharpen etc.
o adjusted ConvolutionKernelFilter to use the new kernel data class
o moved sample kernel data from jiuawt to ConvolutionKernelFilter
o ConvolutionKernelFilter: added convolution kernel data for horizontal and vertical Sobel and Prewitt filters; updated jiuawt with it
+ added class Shear to transform package
+ created ShearDialog in the gui.awt.dialogs package
o modified jiuawt to support shearing

2001-06-17
o ErrorDiffusion: implemented error diffusion; included six error diffusion templates
+ net.sourceforge.jiu.color.quantization: added interface RGBQuantizer and adjusted MedianCut and UniformPaletteQuantizer to implement it
o adjusted jiuawt, UniformPaletteQuantizerDialog, MedianCutDialog and ReduceGrayscaleDialog to support error diffusion
o JiuInfo: changed version from 0.4.0 to 0.5.0

2001-06-16
o released 0.4.0, updated homepage, updated freshmeat.net entry

2001-06-15
o created packages net.sourceforge.jiu.color.quantization and net.sourceforge.jiu.transform and moved some classes there

2001-06-05
+ added Brightness, Contrast and GammaCorrection (all in color); added corresponding menu items to jiuawt
+ added crop operation and dialog

2001-06-01
+ MBMCodec: started codec for the MBM image file format (for the EPOC operating system, which is used in devices like those from Psion)

2001-05-31
+ PalmCodec: started codec for the Palm OS (for handheld devices like the Palm Pilot, HandSpring Visor etc.) native image file format
o RASCodec: palettes are now read correctly (R-G-B)
+ RASCodec: now supports writing paletted images

2001-05-29
o JiuInfo: changed version from 0.3.0 to 0.4.0
o MedianCut: fixed bug that prevent color quantization of images where the number of colors in the palette was only a little smaller than the number of used colors in the truecolor source image; added method canBeSplit to MedianCutNode; this returns true iff a node is a leaf AND if its set of colors has more than one element (otherwise, splitting isn't possible, can only split sets with two or more colors)
o created JIU entry on freshmeat.net; now waiting for acceptance by editor

2001-05-28
o changed the kernel coefficients of the edge detection filter to do a better job;  new coefficients taken from the comp.graphics.algorithms FAQ
o updated jiu.sourceforge.net homepage

2001-05-27
o released JIU 0.3.0

2001-04-04
+ PNMCodec: got a hint from David Reid <djreid@powersurfr.com> that PGM loading doesn't work properly; added loadGrayImage method to do that

2001-02-12
o after yesterday's release of 0.2.0, switched JIU version to 0.3.0
+ added support for several languages for the demo apps (better now than later), classes net.sourceforge.jiu.apps.Strings and net.sourceforge.jiu.apps.StringIndexConstants
o moved system info functionality from net.sourceforge.jiu.apps.jiuawt to net.sourceforge.jiu.apps.SystemInfo
o renamed command line application jiu to jiucli (CLI = command line interface), in package net.sourceforge.jiu.apps
+ added interface JiuInfo with constants on the JIU version, homepage etc.