JIU - Java Imaging Utilities - To do list
=========================================

BUGS

These are things that just don't work correctly. Should have highest priority.

- Some variants of color reduction on 48 bit RGB images do not seem to work in jiuawt. Must be explored further (memory consumption too high with histogram?).
- Resample: downscaling with the Box filter sometimes results in visual artifacts ("bars")
- TIFFDecoderLogLuv: some files are decoded incorrectly (e.g. 14o1.tif), the colors are wrong in parts of the image
- jiuawt: various combinations of Gray16Image / RGB48Image color reduction and dithering algorithms don't work yet
- BorderSampleGenerator: does not do anything yet
- MedianCutContourRemoval: palette entries are swapped, but image quality does not improve

CHANGES / ADJUSTMENTS / REFACTORING

These are things that need some changes although they "kind of work".

- RGBQuantizer: there is no clean definition of what createPalette does; something like init or findPalette must be defined
- TIFFCodec: move reading of TIFFImageFileDirectory objects to a class of its own (it's complex enough and code must be reused for EXIF JPEGs)
- ErrorDiffusionDithering: create a class of its own for dithering templates (instead of int[][])
- ConvolutionKernelFilter: now takes 4 * width * height bytes as temporary buffer, should use less memory; make it an AreaFilterOperation (?)
- jiuawt: AutoDetectColorType should give some feedback (image cannot be reduced / etc.)
- move TIFF fax decoding to FaxDecoder
- TextureAnalysis: separate results of the computation process (data) from the operation (TextureAnalysis); move data into a class of its own
- CoOccurrenceMatrix: add implementation that creates new "counters"--(index, value) pairs--on demand instead of allocating a huge array. This will help supporting 16-bit integer channels.

DOCUMENTATION

Places with too little or misleading or wrong documentation.

- ConvolutionKernelData
- jiuawtapplet

ENHANCEMENTS

Smaller things that are useful and should be added.

- ConvolutionKernelFilter: different edge treatment types: copy border pixels, copy wrap around pixels, copy pixels to output, zero
- IntegerImage: add a method that copies sections to int[] adding border pixels where necessary
- matrix creation should become operation
- better progress notification for AutoDetectColorType and the color quantization operations
- add new color spaces
- jiuawt: add saving of single channels as grayscale images
- jiu.jar should include one or two images in jiu.jar so that jiuawtapplet has something to work on
- PSDCodec: add additional class to read image resources (can be reused for TIFF, PSD resources have a tag of their own)
- add class ClusteredDotDitherDialog and use it in jiuawt
- jiuawt: add preferences dialog
- jiuawt: move progress display from status bar to dialog with an abort button
- jiuawt: add feature to pick language, e.g. Edit | Language | English, German, Spanish
- jiuawt: save settings (EditorState)
- automatically add menu shortcuts, e.g. 'f' for file
- Brightness, Contrast: make it possible to specify different brightness values for each channel
- AreaFilterOperation: optimize it by separating inner and border regions
- OrderedDither: adjust for images with 16 bits per sample, allow truecolor output with more than 8 bpp
- PNMCodec: support max value and PAM
- AwtOperationProcessor: counting colors in RGB48Image quickly leads to OutOfMemory errors with most images; a new Histogram3D implementation which is very conservative with memory allocation could fix this
- TIFFDecoder: support Fill order=2, which requires reordering of bits within a byte https://sourceforge.net/forum/message.php?msg_id=2285841
 
NEW FEATURES

Completely new features, classes or even packages that could be added.

- add support for transparency information to all image data classes and the image codecs
- make ImageToImageOperations work on arbitrary-sized tiles (some operations demand strict sequential processing of lines and therefore do not allow this); good for: multiple threads working on operation, disk-based images (less I/O)
- add some interface for image file metadata
- add support for disk-based image data types (net.sourceforge.jiu.data.disk)
- add transparency information interface
- add transparency classes
- add image data class implementing IntegerImage that allows an arbitrary number of channels and channel types (byte, short, ...)
- add bitblit package that processes transparency information and also does compositing
- add morphology package, in jiuawt Filters | Morphological | Erode, Dilate, Close, Open
- add Riemersma dithering
- add neural net color quantization
- Color | Palette | Sort...
- Color | Palette | Set transparency index...
- Transformation | Rotate (arbitrary angle)...
- add benchmark classes (package?)
- add junit test cases
- jiuawt: add File | Info
- jiuawt: add File | Revert...
- jiuawt: add File | Recent > 1. lena.png / 2. mandrill.ppm

NEW FEATURES THAT REQUIRE JAVA 1.2+

Compatibility with 1.1 was a requirement - so far. I'm thinking about adding features that will require higher Java versions, stored in parentheses.

- ImageCanvas: use bilinear / bicubic interpolation via RenderingHints for displaying images (1.2). On the other hand, an artificially improved image quality may not be the right thing for editing.
- jiuawt: print image (1.2, better 1.3)
- JiuAwtFrame: 	maximize window on start-up (1.4)
- jiuawt: copy image from / to system clipboard (1.4)
- jiuawt: create screen capture (1.3)
- jiuawt: fullscreen mode (1.4)
